<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'user', 'middleware' => 'appauth'], function () use ($router) {
    //inventory request uker
    $router->get('/',function () use ($router) {
        echo "Hello User dong :)";
        return;
    });

    
    $router->post('/login','UsersController@login');
    
    /*
    $router->post('/register','Wbs\NotificationController@register');
    $router->post('/update','Wbs\NotificationController@update');
    $router->post('/logs','Wbs\NotificationController@logs');
    */
});
