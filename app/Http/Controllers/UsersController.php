<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function login(Request $request) {

        $response = (object) [
            "status" => true,
            "data" => [
                "name" => "Adib Adipraditya"
            ]
        ];
        return response()->json($response, 200);
    }

    public function logout(Request $request){
        $response = (object) [
            "status" => true
        ];
        return response()->json($response, 200);
    }

    
}
